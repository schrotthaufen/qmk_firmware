# Set any rules.mk overrides for your specific keymap here.
# See rules at https://docs.qmk.fm/#/config_options?id=the-rulesmk-file
CONSOLE_ENABLE = no
COMMAND_ENABLE = no
ORYX_ENABLE = no
SPACE_CADET_ENABLE = no
CAPS_WORD_ENABLE = yes
WEBUSB_ENABLE = no
AUDIO_ENABLE = no
SEQUENCER_ENABLE = no
MIDI_ENABLE = no
MUSIC_ENABLE = no
SRC = matrix.c
